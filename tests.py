import unittest
from is_palindrome_simple import is_palindrome_simple
from is_palindrome import is_palindrome


class TestIsPalindrome(unittest.TestCase):

    def test_is_palindrome_simple(self):
        self.assertTrue(is_palindrome_simple('шалаш'))
        self.assertTrue(is_palindrome_simple('АББББА'))
        self.assertFalse(is_palindrome_simple('ВАФ'))
        self.assertFalse(is_palindrome_simple('БА'))
        self.assertTrue(is_palindrome_simple('ААА'))
        self.assertTrue(is_palindrome_simple(''))
        self.assertTrue(is_palindrome_simple('АБА'))
        self.assertFalse(is_palindrome_simple('ГАГЫГАГЫ'))

    def test_is_palindrome(self):
        self.assertTrue(is_palindrome('шалаш'))
        self.assertTrue(is_palindrome('шалаш..................'))
        self.assertTrue(is_palindrome('шал-аш'))
        self.assertTrue(is_palindrome('АББ-ББА'))
        self.assertFalse(is_palindrome('В-А,Ф'))
        self.assertFalse(is_palindrome('Б.А'))
        self.assertTrue(is_palindrome('А,--А-А!'))
        self.assertTrue(is_palindrome('....'))
        self.assertTrue(is_palindrome(''))
        self.assertTrue(is_palindrome('!'))
        self.assertTrue(is_palindrome('!А,БА'))
        self.assertFalse(is_palindrome('ГАГЫГАГЫ'))
        self.assertFalse(is_palindrome('Г,А/ГЫ!ГАГЫ.'))


if __name__ == '__main__':
    unittest.main()
