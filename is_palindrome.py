import re


def is_palindrome(value: str) -> bool:
    offset_start, offset_end = 0, 0
    value_length = len(value)

    for index in range(value_length):
        # Для удобства считаем только положительный индекс
        left_index = index - offset_start
        right_index = value_length + offset_end - index - 1

        # Если индексы пересеклись
        if left_index == right_index or left_index > right_index:
            return True

        if not re.match(r'^[a-zA-Zа-яА-Я]$', value[left_index], flags=re.ASCII):
            offset_end += 1
            continue
        if not re.match(r'^[a-zA-Zа-яА-Я]$', value[right_index], flags=re.ASCII):
            offset_start += 1
            continue
        if value[left_index] != value[right_index]:
            return False
    return True
