def is_palindrome_simple(value: str) -> bool:
    for index in range(len(value) // 2):
        if value[index] != value[-1 - index]:
            return False
    return True
